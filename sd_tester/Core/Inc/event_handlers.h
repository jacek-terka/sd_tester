/*
 * event_handlers.h
 *
 *  Created on: 13 mar 2023
 *      Author: jacek terka
 */
#ifndef INC_EVENT_HANDLERS_H_
#define INC_EVENT_HANDLERS_H_

#include <stdbool.h>
#include "stm32f407xx.h"

void buttonHandler(GPIO_TypeDef* port, uint16_t pin, int* button_pushed, void(*event_handler)(void));
void upDownHandler(GPIO_TypeDef* port, uint16_t pin, int* counter, bool* state,
		           void(*up_event_handler)(void), void(*down_event_handler)(void));


void sdHandler(void);
void okButtonHandler(void);
void sdUpHandler(void);
void sdDownHandler(void);
void backButtonUpHandler(void);
void backButtonDownHandler(void);
void upButtonHandler(void);
void downButtonHandler(void);

extern int okButtonPushed;
extern bool sdState;
extern int sdCounter;
extern bool backButtonState;
extern int backButtonCounter;
extern int upButtonPushed;
extern int downButtonPushed;

#endif /* INC_EVENT_HANDLERS_H_ */
