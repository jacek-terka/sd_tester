/*
 * utils.h
 *
 *  Created on: 27 mar 2023
 *      Author: jacek terka
 */
#include <stdbool.h>

#ifndef INC_UTILS_H_
#define INC_UTILS_H_

void initSdCard( void );
void displaySdCardStatus(void);
bool isSdCardInserted( void );
void mountSdCard(void);
void unmountSdCard(void);

void displayFile(void);


#endif /* INC_UTILS_H_ */
