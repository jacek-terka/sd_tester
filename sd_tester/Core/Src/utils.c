/*
 * utils.c
 *
 *  Created on: 27 mar 2023
 *      Author: jacek terka
 */

#include "utils.h"
#include "main.h"
#include "liquidcrystal_i2c.h"
#include "fatfs.h"

FATFS fat_fs;

void initSdCard( void )
{
	if (isSdCardInserted())
	{
		mountSdCard();
	}
}

void displaySdCardStatus()
{
	if ( isSdCardInserted() )
	{
		HD44780_SetCursor(0, 1);
		HD44780_PrintStr("Press OK to test");
	}
	else
	{
		HD44780_SetCursor(0, 1);
		HD44780_PrintStr("Insert card     ");
	}
}

bool isSdCardInserted( void )
{
	return HAL_GPIO_ReadPin(INPUT_SD_GPIO_Port, INPUT_SD_Pin) == GPIO_PIN_SET;
}


void mountSdCard(void)
{
	FRESULT file_result;

	file_result = f_mount( &fat_fs, "", 1 );
	if (file_result != FR_OK )
	{
		  HD44780_SetCursor(0,1);
		  HD44780_PrintStr("Mount error     ");
		  while(1){}
	}
}

void unmountSdCard( void )
{
	FRESULT file_result;

	file_result = f_mount( 0, "", 0 );
	if (file_result != FR_OK )
	{
		  HD44780_SetCursor(0,1);
		  HD44780_PrintStr("Unmount error   ");
		  while(1){}
	}

}

void displayFile(void)
{
	FIL file_handle;
	FRESULT file_result;

	file_result = f_open(&file_handle, "test.txt", FA_READ);
	if ( file_result != FR_OK )
	{
		  HD44780_SetCursor(0,1);
		  HD44780_PrintStr("File open error ");
		  while(1){}
	}

	char read_buffer[20];
	TCHAR* read_result = f_gets((TCHAR*)read_buffer, 20, &file_handle);
	if ( read_result == 0 )
	{
		  HD44780_SetCursor(0,1);
		  HD44780_PrintStr("File read error ");
		  f_close( &file_handle );
		  while(1){}
	}
	else
	{
		  HD44780_SetCursor(0,1);
		  HD44780_PrintStr("                ");
		  HD44780_SetCursor(0,1);
		  HD44780_PrintStr(read_buffer);
		  f_close( &file_handle );
	}
}
