/*
 * event_hadlers.c
 *
 *  Created on: 13 mar 2023
 *      Author: jacek terka
 */

#include "event_handlers.h"
#include "utils.h"
#include "liquidcrystal_i2c.h"
#include "main.h"
#include <stdbool.h>

int okButtonPushed = 0;

bool sdState = false;
int sdCounter = 0;

bool backButtonState = false;
int backButtonCounter = 0;

void buttonHandler(GPIO_TypeDef* port, uint16_t pin, int* button_pushed, void(*event_handler)(void))
{
    if ( HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_SET)
	{
    	if ( *button_pushed < 4 )
    	{
    		++*button_pushed;
    		return;
    	}
    	else if ( *button_pushed < 5 )
    	{
			event_handler();
			++*button_pushed;
    	}
	}
	else
	{
		*button_pushed = 0;
    }
}

void upDownHandler(GPIO_TypeDef* port, uint16_t pin, int* counter, bool* state,
		           void(*up_event_handler)(void), void(*down_event_handler)(void))
{
	if ( *state )
	{
	    if ( HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_RESET)
		{
	    	if ( *counter < 4 )
	    	{
	    		++*counter;
	    		return;
	    	}
	    	else if ( *counter < 5 )
	    	{
				down_event_handler();
				*state = false;
				++*counter;
	    	}
		}
		else
		{
			*counter = 0;
	    }
	}
	else
	{
	    if ( HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_SET)
		{
	    	if ( *counter < 4 )
	    	{
	    		++*counter;
	    		return;
	    	}
	    	else if ( *counter < 5 )
	    	{
				up_event_handler();
				*state = true;
				++*counter;
	    	}
		}
		else
		{
			*counter = 0;
	    }
	}
}

void okButtonHandler(void)
{

	displayFile();
}

void sdUpHandler(void)
{
	HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_SET);
}

void sdDownHandler(void)
{
	HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET);
}

void backButtonUpHandler(void)
{
	HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_SET);
}

void backButtonDownHandler(void)
{
	HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_RESET);
}

void upButtonHandler(void)
{
	HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
}
void downButtonHandler(void)
{
	HAL_GPIO_TogglePin(LD5_GPIO_Port, LD5_Pin);
}
